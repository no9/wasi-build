# © Copyright IBM Corporation 2019.
# LICENSE: Apache 2.0, https://www.apache.org/licenses/LICENSE-2.0

FROM rustlang/rust:nightly

RUN apt-get update -y
RUN apt-get update
RUN apt-get install curl cmake clang build-essential libc6-dev-i386 gcc-multilib git -y

RUN rustup target add wasm32-wasi --toolchain nightly

WORKDIR /build

RUN curl -L https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-8/wasi-sdk-8.0-linux.tar.gz > wasi-sdk.tar.gz

RUN tar xvzf wasi-sdk.tar.gz -C /

RUN rm wasi-sdk.tar.gz

ENV RUST_BACKTRACE 1
ENV PATH "/wasi-sdk-8.0/bin:$PATH"

# ENV LIBCLANG_PATH
RUN git clone --recursive https://github.com/CraneStation/wasmtime.git
WORKDIR /build/wasmtime
RUN cargo update -p libc
RUN cargo install --verbose --path ./
RUN ln -s /wasi-sdk-8.0 /opt/wasi-sdk
RUN rm -fr wasmtime
