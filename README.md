# wasi-build

The [Web Assembly System Interface (WASI)](https://wasi.dev/) is designed to allow the execution of Web Assembly outside of the browser.

This technology currently requires some specific environment configuration that can clash with some setups. 
This project aims to enable people to build and run WASI projects as friction free as possible.


## usage

Based on the [WASI Tutorial](https://github.com/CraneStation/wasmtime/blob/master/docs/WASI-tutorial.md) 

### prereqs
Install [Docker](https://docs.docker.com/install/)

### setup
Clone this repository and build the container image
```
$ git clone https://gitlab.com/no9/wasi-build.git
$ cd wasi-build
$ docker build -t wasi-build .
```

Then look at the C or Rust examples on how you can use the container to build your own code.

### C example
```
$ docker build -t wasi-c-example examples/c
$ docker run wasi-c-example:latest
```

### Rust example

```
$ docker build -t wasi-rust-example examples/rust
$ docker run wasi-rust-example:latest
```

N.B. The image is also available on the [docker hub](https://cloud.docker.com/u/number9/repository/docker/number9/wasi-build).
